﻿using System;

namespace KataBowling
{
    public class Game
    {
        public int RollCounter = 0;
        public int[] pinFall = new int[21];

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public void roll(int pinDown)
        {
            pinFall[RollCounter++] = pinDown;
        }

        bool isStrike(int arrayIndex)
        {
            return pinFall[arrayIndex] == 10;
        }

        bool isSpare(int arrayIndex)
        {
            return pinFall[arrayIndex] + pinFall[arrayIndex + 1] == 10;
        }

        int StrikeBonus(int arrayIndex)
        {
            return pinFall[arrayIndex + 1] + pinFall[arrayIndex + 2];
        }

        int SpareBonus(int arrayIndex)
        {
            return pinFall[arrayIndex + 2];
        }

        public int Score()
        {
            int score = 0;
            int arrayIndex = 0;
            for (int frame = 0; frame < 10; frame++)
            {
                if(isStrike(arrayIndex))
                {
                    score += 10 + StrikeBonus(arrayIndex);
                    arrayIndex += 1;
                }
                else if(isSpare(arrayIndex))
                {
                    score += 10 + SpareBonus(arrayIndex);
                    arrayIndex += 2;
                }
                else
                {
                    score += pinFall[arrayIndex] + pinFall[arrayIndex + 1];
                    arrayIndex += 2;
                }
            }

            return score;
        }
    }
}
