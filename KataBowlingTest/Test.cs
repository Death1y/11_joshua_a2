﻿using NUnit.Framework;
using System;
using KataBowling;

namespace KataBowlingTest
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]
        public void CreateGame()
        {
            game = new Game();
        }

        public void rollMany(int Trys, int pinDown)
        {
            for (int i = 0; i < Trys; i++)
            {
                game.roll(pinDown);
            }
        }

        public void AssertGame(int number)
        {
            Assert.That(game.Score(), Is.EqualTo(number));
        }

        [Test()]
        public void gutterGame()
        {
            rollMany(20, 0);

            AssertGame(0);
        }

        [Test()]
        public void scoreOnesGame()
        {
            rollMany(20, 1);

            AssertGame(20);
        }

        [Test()]
        public void oneSpareGame()
        {
            game.roll(1);
            game.roll(9);
            rollMany(18, 0);

            AssertGame(10);
        }

        [Test()]
        public void oneStrikeGame()
        {
            game.roll(10);
            rollMany(19, 0);

            AssertGame(10);
        }

        [Test()]
        public void PerfectGame()
        {
            rollMany(12, 10);

            AssertGame(300);
        }

        [Test()]
        public void typicalGame()
        {
            game.roll(10);
            game.roll(9);
            game.roll(1);
            game.roll(5);
            game.roll(5);
            game.roll(7);
            game.roll(2);
            game.roll(10);
            game.roll(10);
            game.roll(10);
            game.roll(9);
            game.roll(0);
            game.roll(8);
            game.roll(2);
            game.roll(9);
            game.roll(1);
            game.roll(10);

            AssertGame(187);
        }

        [Test()]
        public void allSpareGame()
        {
            rollMany(21, 5);

            AssertGame(150);
        }

        [Test()]
        public void allTwosGame()
        {
            rollMany(20, 2);

            AssertGame(40);
        }
    }
}
